#!/bin/perl

# LINKS:
# character - http://s2.bbgam.com/game-character/
# this location - http://s2.bbgam.com/game/
# inventory - http://s2.bbgam.com/game-items/
# spellbook - http://s2.bbgam.com/game-spellbook/
# journal - http://s2.bbgam.com/game-journal/#1
# Global info - http://s2.bbgam.com/info/?uid=165037
#


use strict;
use warnings;
use POSIX;

use utf8;
use Tk;
use MIME::Base64;
use Tk::JPEG;
use HTTP::Cookies ();
use LWP::UserAgent ();

my $UserAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/118.0";
$UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/119.0";

my ($RefererDefault,$RefererHero,$RefererGame,$RefererBgrdown,$RefererMenu,$RefererWorld);

my $capcha = 0;
my $capchastring;
my $capchacode;

my $FrameLeft1;
my $FrameLeft11;
my $ButtonExit;
my $FrameLeft12;
my $LabelUserName;
my $LabelUserLvl;
my $LabelUserHp;
my $LabelUserEn;
my $FrameLeft13;

my $FrameLeft2;
my $FrameLeft21;
my $FrameLeft22;

my $FrameLeft3;
my $FrameLeft31;
my $LabelLocation1;
my $ButtonReloadLocation;
my $LabelLocationName1;
my $LabelLocationNumber1;

my $FrameLeft32;
my $MM;

my $FrameLeft33;
my $ButtonNorth;
my $ButtonWest;
my $ButtonEast;
my $ButtonSouth;
my $ButtonUp;
my $ButtonDown;

my $FrameLeft34;
my $FrameLeft35;
my $FrameLeft36;

my $FrameRight1;
my $FrameTop1;
my $FrameTop2;
my $FrameTop3;

my $BodyScroll;
my $BodyText;

my $FrameBottom1;

my $FrameDown1;
my $FrameDown2;

my $location;
my $locationname;
my $locationnumber;
my $locationbody;

my ($hw, $t1, $mw, $req, $html, $lb, $login, $password);
my ($s, $l, $p);
my $auth = 0;
my ($m1, $m2, $m3, $m4, $b1);
my $url1 = 'http://s.bbgam.com/game/login.php';
my $url2 = "http://s2.bbgam.com/game/";
my $urlserver;
my ($navi, $searchhere);
my @nav = (0,0,0,0,0,0,0);
my @navn = (0,0,0,0,0,0,0);
# 0 - up, 1 - down, 2 - N, 3 - S!, 4 - w, 5 - e, 6 - search here?

my $ShowMap = 0;
my $border = 6;
my $size = 50;
my @MP = (0 .. 24);
my @MPN = (0 .. 24);

my $UserUpdate=0;
my $UserName ="";
my $UserLvl ="";
my $UserLvlP ="";
my $UserHp ="";
my $UserHpP ="";
my $UserEn ="";
my $UserEnP ="";

my $NpcDialog = 0;

my $fight = "";
my @fights; #каст или номер скила в списке заклинаний
my @fighta; #ссылка на удар
my @fightn; #название удара
my $fightr; #соль в удар
my @fightl; #ссылка на удар
my @fighth; #номер ссылки через js
my @fightt; #дырявый массив для простоты


my $jar = HTTP::Cookies->new(
  file     => "./skazanie.cookies",
  autosave => 1,
);
my $ua  = LWP::UserAgent->new(
    cookie_jar        => $jar,
    protocols_allowed => ['http', 'https'],
    timeout           => 10,
    agent             => $UserAgent,

);

# $ua->proxy(['https','http'], 'http://0.0.0.0:0');
$ua->ssl_opts( verify_hostname => 0, );

$ua->default_header('Accept' => "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8");
$ua->default_header('Accept-Language' => "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
$ua->default_header('Accept-Encoding' => "gzip, deflate");

 $mw = MainWindow->new();
 $mw->title("Сказание - Web game - Perl/TK client");
$hw = $mw->Label(-text => "Выполните вход в другом окне")->pack( );
if($auth == 0){
  do_login_window();
}

MainLoop;
exit;

sub cycle {
analyze_nav_html();
draw_nav_but();
show_nav();
analyze_stat_html();
analyze_location_html();

analyze_mm_html();
analyze_body_html();

analyze_capcha_html();


 show_MM();
 show_stat();
 skip_page();
 show_body();
 analyze_fight_html();

}

sub analyze_fight_html {
   return if ($html !~ m/Выбeритe дeйствие для cлeдующeго paундa:/);

   my $startstr ;
   my $endstr ;
   my $startpos;
   my $endpos;
   my $tmp = $html;
   my $t;
   my @g;
   my $body;
   my $q;


    $startstr = "Раунд N ";
    $endstr = "<";
    $startpos = index($tmp,$startstr);
    $endpos = index($tmp, $endstr,$startpos);
    $t = substr($tmp,$startpos,($endpos - $startpos));
    $FrameLeft33->Label(-text=>"$t")->pack();

      $startstr = 'sxl(1,';
      $endstr = '"));</';
      $startpos = index($tmp,$startstr);
      $endpos = index($tmp, $endstr,$startpos);
      $t = substr($tmp,($startpos + 6),($endpos - $startpos - 6));
      $t =~ s/\"//g;
       @g = split('[)][)][;]sxl[(]1[,]',$t);
       for my $i (0 .. $#g) {
         my ($a,$b) = split(',',$g[$i]);
         my ($c,$d) = split('[+]unescape[(]',$b);
         $d =~ s/%//g;
         $d =~ s/([[:xdigit:]]{2})/chr(hex($1))/eg;
         $b = $c . $d;
         $fightl[$i] = $b;
         $fighth[$i] = $a;
         $fightt[$a] = $b;
       }

     $startstr = "<lI>";
     $endstr = '3дoрoвьe';
     $startpos = index($tmp,$startstr);
     $endpos = index($tmp, $endstr,$startpos);
     $t = substr($tmp,($startpos + 4),($endpos - $startpos - 4));
     @g = split("<[lL][iI]>",$t);
     for my $i (0 .. $#g){
      next if ( $g[$i] !~ m/[hH][rR][eE][fF][=]/ );
      my ($a,$b,$c,$d,$e)=split('</[aA]',$g[$i]);
      $fightn[$i] = $a;
      $fightn[$i] =~ s/<.*>//;
      ($a,$b,$c,$d,$e)= split('"',$a);
      $fights[$i]=substr($c,1) if ("s" eq substr($c,0,1));
      my ($h,$j,$k) = split('=',$b);
      if ('?make=3&r=' eq substr($b,0,10)) {
        $fightr=$k;
        $fighta[$i]=$b;
        next;
      }
      $fighta[$i]=substr($b,14,(length($b) - 15)) if ('javascript:t1' eq substr($b,0,13));
      $fighta[$i]=~s/[)]//;
     }

     clearFrame($FrameLeft33);
     clearFrame($FrameLeft34);
     clearFrame($FrameLeft35);

      for my $i (0 .. $#g){
        next if ( $g[$i] !~ m/[hH][rR][eE][fF][=]/ );
        my $name =$fightn[$i];
        my $salt =$fightr;
        my $kick =$fighta[$i];
        my $cast =$fights[$i];

        if ( (defined $fighta[$i]) and ("$fighta[$i]" =~ m/^[?]/) ) {
          $FrameLeft33->Button(-text=>"$fightn[$i]",-command=>sub{goKick("$fighta[$i]");})->pack();
          next;
        }
        $FrameLeft33->Button(-text=>"$fightn[$i]",-command=>sub{goKick("?$fightt[$fighta[$i]]");})->pack();
      }

     $startstr = "</scrIPT>";
     $endstr = "<[tT][aA][bB]";
     $startpos = index($tmp,$startstr,$endpos);
     $endpos = index($tmp, $endstr,$startpos);
     $t = substr($tmp,$startpos,($endpos - $startpos));
     $BodyText->delete("1.0",'end');
     $BodyText->insert('end',"$t");

      $startstr = "Противники";
      $endstr = ":";
      $startpos = index($tmp,$startstr,$endpos);
      $endpos = index($tmp, $endstr,$startpos);
      $t = substr($tmp,$startpos,($endpos - $startpos));
      $t =~ s/<\/b>//;
      $FrameLeft34->Label(-text=>"$t")->pack();

      $startstr = '<small>';
      $endstr = "Союзники";
      $startpos = index($tmp,$startstr,$endpos);
      $endpos = index($tmp, $endstr,$startpos);
      $t = substr($tmp,($startpos + 7),($endpos - $startpos - 7));
      $t =~ s/\/r//;
      @g = split ("<br>",$t);
      foreach (@g){
      next if ( substr($_,0,2) ne "<a" );
        $startstr = 'blank>';
        $endstr = '</';
        $startpos = index($_,$startstr);
        $endpos = index($_, $endstr,$startpos);
        $t = substr($_,($startpos + 6),( -8));
        $t =~ s/<\/a>//;
        $FrameLeft34->Label(-text=>"$t")->pack();
      }

     $startstr = "Союзники";
     $endstr = ":";
     $startpos = index($tmp,$startstr,$endpos);
     $endpos = index($tmp, $endstr,$startpos);
     $t = substr($tmp,$startpos,($endpos - $startpos));
     $t =~ s/<\/b>//;
     $FrameLeft35->Label(-text=>"$t")->pack();

     $startstr = "<b>";
     $endstr = "<p>";
     $startpos = index($tmp,$startstr,$endpos);
     $endpos = index($tmp, $endstr,$startpos);
     $t = substr($tmp,($startpos + 3),($endpos - $startpos - 3));
     @g = split ("<br>",$t);
     foreach (@g){
     next if ( substr($_,0,2) ne "<a" );
       $startstr = ">";
       $endstr = "<";
       $startpos = index($_,$startstr);
       $endpos = index($_, $endstr,$startpos);
       $t = substr($_,($startpos + 1),($endpos - $startpos - 1));
       $FrameLeft35->Label(-text=>"$t")->pack();
     }
}

sub draw_nav_but {
  $ButtonNorth = $FrameLeft33->Button(-height=>2,-width=>15, -text=>"Север", -state=>"disabled",-command=>sub{goGo(2);})->grid(-row=>1,-column=>1,-rowspan=>1,-columnspan=>1,-sticky=>"nesw");
  $ButtonWest  = $FrameLeft33->Button(-height=>3,-width=>7,-text=>"Запад", -state=>"disabled",-command=>sub{goGo(4);})->grid(-row=>2,-column=>0,-rowspan=>1,-columnspan=>1,-sticky=>"nesw");
  $ButtonEast  = $FrameLeft33->Button(-height=>3,-width=>7,-text=>"Восток", -state=>"disabled",-command=>sub{goGo(5);})->grid(-row=>2,-column=>2,-rowspan=>1,-columnspan=>1,-sticky=>"nesw");
  $ButtonSouth = $FrameLeft33->Button(-height=>2,-width=>15,-text=>"Юг", -state=>"disabled",-command=>sub{goGo(3);})->grid(-row=>3,-column=>1,-rowspan=>1,-columnspan=>1,-sticky=>"nesw");

  $ButtonDown = $FrameLeft33->Button(-height=>2,-width=>7,-text=>"Вниз", -state=>"disabled",-command=>sub{goGo(1);})->grid(-row=>2,-column=>1,-rowspan=>1,-columnspan=>1,-sticky=>"es");
  $ButtonUp   = $FrameLeft33->Button(-height=>2,-width=>7,-text=>"Вверх", -state=>"disabled",-command=>sub{goGo(0);})->grid(-row=>2,-column=>1,-rowspan=>1,-columnspan=>1,-sticky=>"nw");
}

sub show_body {
 return if ($html =~ m/Выбeритe дeйствие для cлeдующeго paундa:/);
 return if (not defined $locationbody);

  my $tmp = $locationbody;

 $tmp =~ s/<[sS][Cc][Rr][Ii][Pp][Tt]>[^<]*<\/[sS][Cc][Rr][Ii][Pp][Tt]>//g;

 my $startstr;
 my $endstr;
 my $startpos;

   $startstr = '<table';
   $endstr = '</table>';
   $startpos = index($tmp,$startstr);
  if ($startpos > "-1") {
   my $endpos = index($tmp,$endstr);
   my $t1 = substr($tmp, 0, $startpos);
   my $t2 = substr($tmp, $startpos , ($endpos - $startpos + 8));
   my $t3 = substr($tmp, ($endpos + 8), (length($tmp) - $endpos + 8));
   $tmp = $t1 . "<TABLE>\n" . $t3;
  }

  $startstr = '<script';
  $endstr = '</script>';
  $startpos = index($tmp,$startstr);
  if ($startpos > "-1") {
    my $endpos = index($tmp,$endstr);
    my $t1 = substr($tmp, 0, $startpos);
    my $t2 = substr($tmp, $startpos , ($endpos - $startpos + 9));
    my $t3 = substr($tmp, ($endpos + 9), (length($tmp) - $endpos + 9));
    $tmp = $t1 . $t3;
  }


 $tmp =~ s/\n//g;
 $tmp =~ s/\r//g;
 $tmp =~ s/<br>/\n/g;
 $tmp =~ s/<p>/\n/g;
 $tmp =~ s/<\/p>/\n/g;
 $tmp =~ s/<div[^<]*>/\n/g;
 $tmp =~ s/<\/div>/\n/g;
 $tmp =~ s/<[lL][iI]>/\n/g;
 $tmp =~ s/<nobr>/\n/g;
 $tmp =~ s/<\/nobr>/\n/g;
 $tmp =~ s/&#187;/\n/g;
 $tmp =~ s/&#171;/\n/g;
 $tmp =~ s/&nbsp;/\n/g;

 $tmp =~ s/<center>/\n/g;
 $tmp =~ s/<\/center>/\n/g;

 $tmp =~ s/<[uU][lL]>/\n/g;
 $tmp =~ s/<\/[uU][lL]>/\n/g;

 $tmp =~ s/<[bB]>/<!!/g;
 $tmp =~ s/<\/[bB]>/!!>/g;

 $tmp =~ s/<[iI]>/<#/g;
 $tmp =~ s/<\/[iI]>/#>/g;

 $tmp =~ s/<font [^<]*>/\n/g;
 $tmp =~ s/<\/font>/\n/g;

 my @links;
 my @strings;
 my $i = 0;
 my $pos = 0;
 while (++$i) {
  my $startstr = "href=";
  my $endstr = " ";
  my $startpos = index($tmp,$startstr,$pos);
  if ($startpos < 0){$i = $i-2; last;}
  my $check=substr($tmp,($startpos+length($startstr)),1);
  if($check eq "'"){
    $startstr = "href='";
    $endstr = "'";
  }
  if($check eq '"'){
    $startstr = 'href="';
    $endstr = '"';
  }

  my $endpos = index($tmp,$endstr,($startpos+length($startstr)));
  $links[$i-1] = substr($tmp,($startpos+length($startstr)),($endpos-$startpos-length($startstr)));
  $pos=($endpos+1);

  $startstr = '>';
  $endstr = '<';
  $startpos = index($tmp,$startstr,$pos);
  $endpos = index($tmp,$endstr,($startpos+length($startstr)));
  $strings[$i-1] = substr($tmp,($startpos+length($startstr)),($endpos-$startpos-length($startstr)));

 }
 my $b=0;
 for my $a (0 .. $i) {
 my $r = ( floor($b / 2) );
 my $c = ( $b - ($r * 2) );

print "New link: $links[$a]\n" if ( ($links[$a] !~ m"^/game/") and ($links[$a] !~ m"^[?]") and ($links[$a] !~ m"^/game-npc/") and ($links[$a] !~ m"^/info/") and ($links[$a] !~ m"^/help/") and ($links[$a] !~ m"^/i/map") and ($links[$a] !~ m"^/i/user-map") and ($links[$a] !~ m"^javascript:top.hero.ma2[(]") and ($links[$a] !~ m"^/b/bid.php[?]bid=")  );

 print "LINK $links[$a]\n";



 if($links[$a] =~ m"^javascript:top.hero.ma2[(]"){
  my ($amid,$cc) = split(',',substr($links[$a],24,length($links[$a])-26));
  $FrameTop3->Button(-command=>sub {goKick("?amid=$amid&c=$cc");},-text=>"$strings[$a]")->grid(-row=>"$r",-column=>"$c");
  $b++;
  next;
 }

 next if ( ($links[$a] !~ m"^/game") and ($links[$a] !~ m"^[?]") );
  $FrameTop3->Button(-command=>sub {goMake($links[$a]);},-text=>"$strings[$a]")->grid(-row=>"$r",-column=>"$c");
  $b++;
 }

 $tmp =~ s/\n\n\n\n\n\n\n/\n\n/g;
 $tmp =~ s/\n\n\n\n\n\n/\n\n/g;
 $tmp =~ s/\n\n\n\n\n/\n\n/g;
 $tmp =~ s/\n\n\n\n/\n\n/g;
 $tmp =~ s/\n\n\n/\n\n/g;

 $tmp =~ s/<[aA] [^<]*>/<</g;
 $tmp =~ s/<\/[aA]>/>>/g;

 $BodyText->delete("1.0",'end');
 $BodyText->insert('end',"$tmp");

}

sub goMake {
my ($a,$b) = @_;
  my $c=substr($a,0,1);
  $b="/game/";
  $b="" if ($c eq "/");
  $b = "/game-npc/" if ( ($NpcDialog == 1) and ($c eq "?") );
  $NpcDialog=1 if ($a =~ m/^\/game-npc/);
  $NpcDialog=0 if ( ($a =~ m/^\/game\//) or ($b eq "/game/") );

#    print "GO:" . $urlserver . $b . $a ."\n";
  $ua->default_header('Referer' => "$RefererGame");
  $req = $ua->get($urlserver . $b . $a);
  $ua->default_header('Referer' => "$RefererDefault");
  jar();
  cycle();
}

sub skip_page() {
  my @str = ("Вы атакованы!", "Сражение завершено");
  foreach (@str){
    if ($html =~ m/$_/) {
      $navi = 1;
      goGame();
    }
  }
}

sub analyze_stat_html {
  $UserUpdate = 0;
  my $startstring = "o.i7(";
  my $endstring = ');';
  my $startpos = index($html, $startstring);
  if ($startpos > "-1"){
    $UserUpdate++;
    my $endpos = index($html, $endstring, $startpos);
    my $tmp = substr($html,($startpos + 5),($endpos - $startpos - 6));
    my ($a,$b,$c,$d) = split (",",$tmp);
    $c = substr($c,4,(length($c) - 5));
    $d =~ s/"//g;
    $UserHpP = $a;
    $UserEnP = $b;
    $UserHp = $c;
    $UserEn = $d;
  }
  $startstring = 'i5("';
  $endstring = ')';
  $startpos = index($html, $startstring);
  if ($startpos > "-1"){
    $UserUpdate++;
    my $endpos = index($html, $endstring, $startpos);
    my $tmp = substr($html,($startpos + 4),($endpos - $startpos - 4));
    my ($a,$b,$c) = split (",",$tmp);
    $a =~ s/"//g;
    $UserLvlP = $a;
    $UserLvl = $b;
    # $c - Что это? задержка???
  }
}

sub show_stat {
  return if ($UserUpdate == 0);
  $LabelUserLvl->configure(-text=>"Lvl:\t$UserLvl\t$UserLvlP%");
  $LabelUserHp->configure(-text=>"Hp: \t$UserHp\t$UserHpP%");
  $LabelUserEn->configure(-text=>"En: \t$UserEn\t$UserEnP%");
  $UserUpdate = 0;
}

sub analyze_location_html {
  $locationbody = "";
  return if ($html =~ m/Выбeритe дeйствие для cлeдующeго paундa:/);
  my $startstring = '<span id=a1>';
  my $endstring = 'document.write';
  my $startpos = index($html, $startstring);
  if ($startpos < "0"){$location = 0; return;}else{$location = 1;}
  my $endpos = index($html, $endstring, $startpos);
  my $tmp = substr($html,$startpos,($endpos - $startpos));

  $startstring = '<a href="/i/">';
  $endstring = '</a>';
  $startpos = index($tmp, $startstring);
  $endpos = index($tmp,$endstring,$startpos);
  $locationname = substr($tmp,($startpos + 14),($endpos - $startpos - 14));

  $startstring = '</b> (';
  $endstring = ') <a';
  $startpos = index($tmp, $startstring);
  $endpos = index($tmp,$endstring,$startpos);
  $locationnumber = substr($tmp,($startpos + 6),($endpos - $startpos - 6));

  $startstring = '<div id="speech">';
  $endstring = '</td></tr></table>';
  $startpos = index($html, $startstring);
  $endpos = index($html,$endstring,$startpos);
  $locationbody = substr($html,($startpos + 17),($endpos - $startpos -17));
  }

sub analyze_body_html {
  return if ($html =~ m/Выбeритe дeйствие для cлeдующeго paундa:/);

  my $startstring = '<div id="speech">';
  my $endstring = '</td></tr></table>';
  my $startpos = index($html, $startstring);
  $locationbody="" if ($startpos <0);
  my $endpos = index($html,$endstring,$startpos);
  $locationbody = substr($html,($startpos + 17),($endpos - $startpos -17));
  $navi = 1;
}

sub show_nav{
  if (defined $locationname){$LabelLocationName1->configure(-text=>$locationname);}
  if (defined $locationnumber) {$LabelLocationNumber1->configure(-text=>$locationnumber);}
  if ($navi eq 0){
     $ButtonNorth->configure(-state=>"disabled");
     $ButtonWest->configure(-state=>"disabled");
     $ButtonEast->configure(-state=>"disabled");
     $ButtonSouth->configure(-state=>"disabled");
     $ButtonUp->configure(-state=>"disabled");
     $ButtonDown->configure(-state=>"disabled");
  }else{

    if ($nav[0] eq ""){
      $ButtonUp->configure(-state=>"disabled");
    }else{
      $ButtonUp->configure(-state=>"normal");
    }
    if ($nav[1] eq ""){
      $ButtonDown->configure(-state=>"disabled");
    }else{
      $ButtonDown->configure(-state=>"normal");
    }
    if ($nav[2] eq ""){
      $ButtonNorth->configure(-state=>"disabled");
    }else{
      $ButtonNorth->configure(-state=>"normal");
    }
    if ($nav[3] eq ""){
      $ButtonSouth->configure(-state=>"disabled");
    }else{
      $ButtonSouth->configure(-state=>"normal");
    }
    if ($nav[4] eq ""){
      $ButtonWest->configure(-state=>"disabled");
    }else{
      $ButtonWest->configure(-state=>"normal");
    }
    if ($nav[5] eq ""){
      $ButtonEast->configure(-state=>"disabled");
    }else{
      $ButtonEast->configure(-state=>"normal");
    }
  }
}

sub useCookies{
  $s = $lb->get($lb->curselection( ));
  $urlserver = "http://s$s.bbgam.com";
  $RefererDefault = "http://s$s.bbgam.com/game/";
  $RefererGame = "http://s$s.bbgam.com/game/";
  $RefererWorld = "http://s$s.bbgam.com/in/world.php";
  $url2 = "http://s$s.bbgam.com/game/";
  $req = $ua->get("http://s$s.bbgam.com/in/world.php");
  jar();
  my ($a,$b,$c) = split("'",$html);
  if ($b =~ m"^http://s.*"){$auth = 1;}else{print "Auth problem\n"; return;}
  $req = $ua->get($b);
  jar();
  $req = $ua->get("http://s$s.bbgam.com/in/world.php");
  jar();
  $ua->default_header('Referer' => "$RefererWorld");
  SetRef();
  $req = $ua->get($url2);
  jar();
  $ua->default_header('Referer' => "$RefererDefault");
  $ua->default_header('Upgrade-Insecure-Requests' => "1");

  clean_menu();
  cycle();
}

sub go_login {
  $s = $lb->get($lb->curselection( ));
  $urlserver = "http://s$s.bbgam.com";
  $RefererDefault = "http://s$s.bbgam.com/game/";
  $RefererGame = "http://s$s.bbgam.com/game/";
  $RefererWorld = "http://s$s.bbgam.com/in/world.php";
  $l = $login->get();
  $p = $password->get();
  $url2 = "http://s$s.bbgam.com/game/";
  $req = $ua->post('http://s.bbgam.com/game/login.php', ['s'=>$s, 'name'=>"$l", 'pass' => "$p", 'd' => "OHwzfDJ8M3wyNDk0OXwzMzkyfDE5MDh8MTM1NXwxMTYzfDEuMTMyMDc1NDcxNjk4MTEzMnwwfEV1cm9wZS9LeWl2fFJhZGVvbiBSOSAyMDAgU2VyaWVz"]);
  jar();
  $UserName = $l;
  ($l,$p) = 0;
  my ($a,$b,$c) = split("'",$html);

  if ($b =~ m"^http://s.*"){$auth = 1;}else{print "Auth problem\n"; return;}
  $req = $ua->get($b);
  jar();
  $req = $ua->get("http://s$s.bbgam.com/in/world.php");
  jar();
  $ua->default_header('Referer' => "$RefererWorld");
  SetRef();
  $req = $ua->get($url2);
  jar();
  $ua->default_header('Referer' => "$RefererDefault");
  $ua->default_header('Upgrade-Insecure-Requests' => "1");

  clean_menu();
  cycle();
}

sub SetRef {
  my $h = $html;
  my $startstring = 'frame name="menu" src="';
  my $endstring = '"';
  my $startpos = index($html,$startstring);
  my $endpos = index($html,$endstring,($startpos + 23));
  my $tmp = substr($html,($startpos + 23),($endpos - $startpos - 23));
  $RefererMenu = $urlserver . '/in/' . $tmp ;
  $req = $ua->get($RefererMenu);
  jar();

  $html=$h;
  $startstring = 'frame name="bgrdown" src="';
  $endstring = '"';
  $startpos = index($html,$startstring);
  $endpos = index($html,$endstring,($startpos + 26));
  $tmp = substr($html,($startpos + 26),($endpos - $startpos - 26));
  $RefererBgrdown = $urlserver . '/in/' . $tmp ;
  $req = $ua->get($RefererBgrdown);
  jar();

  $html=$h;
  $startstring = 'frame name="hero" src="';
  $endstring = '"';
  $startpos = index($html,$startstring);
  $endpos = index($html,$endstring,($startpos + 23));
  $tmp = substr($html,($startpos + 23),($endpos - $startpos - 23));
  $RefererHero = $urlserver . '/in/' . $tmp ;
  $req = $ua->get($RefererHero);
  jar();
  analyze_stat_html();
  $UserUpdate =1;
  $html=$h;

#   http://s2.bbgam.com/in/chat-0.html
#   http://s2.bbgam.com/in/bgrleft2.html
#   http://s2.bbgam.com/in/smiles-0.php
#   http://s2.bbgam.com/in/bgrright2.html
#   http://s2.bbgam.com/in/form4.php?l=28&cc=5d98b1a652e65a2055e001&fs=16&ui=165019&ch=1
#   http://s2.bbgam.com/b/spend.php?news=1
}

sub analyze_nav_html {
  my $startstring = 'script>document.write(unescape("';
  my $endstring = '"';
  my $startpos = index($html,$startstring);
  my ($endpos,$str);
  if ($startpos > "-1"){
    $navi = 1;
    $endpos = index($html,$endstring,($startpos+32));
    $str = substr($html,($startpos+32),($endpos - $startpos - 32));
    $str =~ s/%//g;
    $str =~ s/([[:xdigit:]]{2})/chr(hex($1))/eg;
  }else{
    $navi = 0;
    return;
  }
  for ( 0 .. $#nav ) {
    my $startstring = "(1,$_,";
    my $startpos = index($str,$startstring);
	my $endstring = ")";
	my $endpos = index($str,$endstring,$startpos);
	my $string = substr($str,($startpos + 5),($endpos - $startpos - 6));
	$startstring = "(";
	$startpos = index($string,$startstring);
	if ($startpos > "-1") {
      $string = substr($string,($startpos + 2));
    }
	if (substr($string,0,1) eq "%"){
      $string =~ s/%//g;
      $string =~ s/([[:xdigit:]]{2})/chr(hex($1))/eg;
      	$nav[$_] = $string;
    }else{
      $nav[$_] = "";
    }
  }
  $startstring = "xarrows(";
  $startpos = index($str,$startstring);
  $endstring = ")";
  $endpos = index($str,$endstring,$startpos);
  $searchhere = substr($str,($startpos + 8),($endpos - $startpos - 8));
}

sub goGo {
  my ($a,$b) = @_;
#   print "a: $a b: $b \n";
  $ua->default_header('Referer' => "$RefererHero");
  $req = $ua->get($urlserver . "/game/?" . $nav[$a]);
  $ua->default_header('Referer' => "$RefererDefault");
  $NpcDialog = 0;
  jar();
  cycle();
}

sub goKick {
  my ($a,$b) = @_;
#    print "goKick: " . $urlserver . "/game/" . $a . "\n";
  $ua->default_header('Referer' => "$RefererGame");
  $req = $ua->get($urlserver . "/game/" . $a);
  $ua->default_header('Referer' => "$RefererDefault");
  $NpcDialog = 0;
  jar();
  cycle();
}

sub bb {
  $ua->default_header('Referer' => "$RefererMenu");
  $req = $ua->get("http://s$s.bbgam.com/game/lo.php");
  exit 1;
}

sub clean_menu {
  $t1->destroy;
  $hw->destroy;
  $mw->minsize(1200, 700);

  $FrameDown1 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side=>'bottom',-fill=>'x');
  $FrameDown2 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side=>'bottom',-fill=>'x');

  $FrameLeft1 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'left', -fill => 'y');
  $FrameLeft11 = $FrameLeft1->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');
  $FrameLeft12 = $FrameLeft1->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');
  $LabelUserName = $FrameLeft12->Label(-text=>"$UserName")->pack();
  $LabelUserLvl  = $FrameLeft12->Label(-foreground=>"brown",-text=>"Lvl:\t$UserLvl\t($UserLvlP%)", -anchor=>'w')->pack(-side => 'top', -fill => 'x');
  $LabelUserHp   = $FrameLeft12->Label(-foreground=>"red",-text=>"Hp: \t$UserHp\t($UserHpP%)", -anchor=>"w")->pack(-side => 'top', -fill => 'x');
  $LabelUserEn   = $FrameLeft12->Label(-foreground=>"blue",-text=>"En: \t$UserEn\t($UserEnP%)", -anchor=>"w")->pack(-side => 'top', -fill => 'x');
   $FrameLeft13 = $FrameLeft1->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');

  $FrameLeft2 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'left', -fill => 'y');
  $FrameLeft21 = $FrameLeft2->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');
  $FrameLeft22 = $FrameLeft2->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');

  $FrameLeft3 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'left', -fill => 'y');
  $FrameLeft31 = $FrameLeft3->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');
  $FrameLeft32 = $FrameLeft3->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');
  $FrameLeft33 = $FrameLeft3->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');
  $FrameLeft34 = $FrameLeft3->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');
  $FrameLeft35 = $FrameLeft3->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side => 'top', -fill => 'x');

  $ButtonExit = $FrameLeft11->Button(-text => "Выйти из игры", -command => \&bb)->pack();
#   $LabelLocation1 = $FrameLeft31->Label(-text=>"Локация:")->pack();
  $ButtonReloadLocation = $FrameLeft31->Button(-text => "Перемещение", -command => \&goGame)->pack();
  $LabelLocationName1 = $FrameLeft31->Label(-text=>"Название")->pack();
  $LabelLocationNumber1 = $FrameLeft31->Label(-text=>"Номер")->pack();

#   $FrameLeft33->Label(-text=>"Перейти:")->grid(-row=>0,-column=>0,-rowspan=>1,-columnspan=>3);


#   $FrameRight1 = $mw->Frame(-borderwidth => 1, -relief => 'groove')-pack(-side=>'right', -fill =>'y');

  $FrameTop1 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side=>'top',-fill=>'x');

  $FrameTop2 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side=>'top',-fill=>'x');
  $FrameTop3 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side=>'top',-fill=>'x');

  $FrameBottom1 = $mw->Frame(-borderwidth => 1, -relief => 'groove')->pack(-side=>'bottom',-fill=>'y');

  $BodyScroll = $FrameTop2->Scrollbar()->pack(-side => 'left', -fill => 'y');
  $BodyText = $FrameTop2->Text(-yscrollcommand => ['set' => $BodyScroll],-height=>14,-wrap=>'word', -font=>[-size=>"9"] )->pack(-fill=>'x',-side=>"left");
  $BodyScroll->configure(-command => ['yview' => $BodyText]);
}

sub analyze_capcha_html {
  my $startstring = '/game/robot-img.php';
  my $startpos = index($html,$startstring);
  if ($startpos > "-1") {
    $capcha=1;
    my $endstring = ")";
    my $endpos = index($html,$endstring,$startpos);
    my $capchaurl = $urlserver . substr($html,$startpos,($endpos - $startpos));
    my $tmp = $html;
    $ua->default_header('Accept' => 'image/avif,image/webp,*/*');
    $req = $ua->get($capchaurl);
    jar();
    my $img = encode_base64($html);
    $html = $tmp;
    $ua->default_header('Accept' => "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8");
    $t1 = $mw->Toplevel( );
    $t1->title("Введите капчу:");
    my $photo = $t1->Photo(-format => 'jpeg', -data => $img);
    my $l = $t1->Label(-image => $photo)->pack();
    $startpos = $endpos;
    $startstring = 'name="';
    $startpos = index($html,$startstring,($startpos + 6));
    $endstring = '"';
    $endpos = index($html,$endstring,($startpos + 6));
    $capchastring = substr($html,($startpos + 6),($endpos - $startpos - 6));
    $capchacode = $t1->Entry()->pack();
    $t1->Button(-text=>"Отправить", -command=>\&sendCapcha)->pack();
    $t1->Label(-text=>"Если нечитаемо (но не увлекайтесь!):");
    $t1->Button(-text=>"Обновить", -command=>\&closeCapcha)->pack();
  }else{
  $capcha=0;
  return;
  }
}

sub sendCapcha {
my $c = $capchacode->get;
$req = $ua->post($urlserver . "/game/robot-test.php",["$capchastring"=>"$c"]);
jar();
closeCapcha();
}

sub clearFrame {
  my ($a,$b) = @_;
  return if (! defined $a);
  my @kids = $a->children;
  foreach (@kids) {
    $_->destroy if (defined $_);
  }
}

sub goGame {
  $ua->default_header('Referer' => "$RefererMenu");
  $req = $ua->get($urlserver . "/game/");
  $NpcDialog = 0;
  jar();
  cycle();
}

sub closeCapcha{
if (defined $t1){$t1->destroy;}
goGame();
}

sub do_login_window {
    $t1 = $mw->Toplevel( );
    $t1->title("Вход в игру");
    $m1 = $t1->Label(-text => "Для подключения к серверу Сказания, необходимо выполнить следующие действия")->pack( );
    $m3 = $t1->Label(-text => "Ввести имя персонажа:")->pack( );
    $login = $t1->Entry()->pack();
    $m4 = $t1->Label(-text => "Ввести пароль (секретное слово):")->pack( );
    $password = $t1->Entry(-show=>'?')->pack();
    $m2 = $t1->Label(-text => "Указать сервер:")->pack( );
    $lb = $t1->Listbox(-height=>4, -selectmode => "single")->pack( );
    $lb->insert('end', 1, 2, 3, 4);
    $b1 = $t1->Button(-text => 'Войти в игру', -command => \&go_login)->pack();
    $t1->Button(-text => "Закрыть программу", -command => "exit" )->pack;
    $t1->Button(-text => "Использовать Куку", -command => \&useCookies )->pack;
     $t1->deiconify( );
     $t1->raise( );
}

sub jar {
$jar->extract_cookies($req);
$html = $req->decoded_content;
}

sub show_MM {
  return if $ShowMap == 0;
  $MM->destroy if (defined $MM);
  $MM = $FrameLeft32->Canvas(-scrollregion=>[0,0,($size*5),($size*5)],-borderwidth => 1, -relief => 'groove',-height=>($size * 5),-width=>($size * 5))->pack();
  for my $i (0 .. $#MP) {
    my $x = ($size * ( $i - floor($i / 5) * 5 ) );
    my $y = ($size * floor($i / 5) ) ;
    my $backcolor = "white";
    $backcolor = "#bbbbbb" if $MP[$i] & 128;
    $backcolor = "#dddddd" if $MP[$i] & 64;
    $MM->createPolygon(($x+$border*2),$y,($x+$size-$border*2),$y,($x+$size/2),($y+$size/2), -fill=>"#ffff00", -width=>0) if $MP[$i] & 4;
    $MM->createPolygon(($x),($y+$border*2),($x),($y+$size-$border*2),($x+$size/2),($y+$size/2), -fill=>"#ffff00", -width=>0) if $MP[$i] & 1;
    $MM->createPolygon(($x+$size),($y+$border*2),($x+$size),($y+$size-$border*2),($x+$size/2),($y+$size/2), -fill=>"#ffff00", -width=>0) if $MP[$i] & 2;
    $MM->createPolygon(($x+$border*2),($y+$size),($x-$border*2+$size),($y+$size),($x+$size/2),($y+$size/2), -fill=>"#ffff00", -width=>0) if $MP[$i] & 8;
    if ($MP[$i]){
      $MM->createOval(($x+$border-$border/3),($y+$border-$border/3),($x+$size-$border+$border/3),($y+$size-$border+$border/3), -fill=>"#FFC0CB", -width=>2,-outline=>"#FF00FF") if $i == 12;
      $MM->createRectangle(($x+$border),($y+$border),($x+$size-$border),($y+$size-$border), -fill=>"$backcolor", -width=>1,-outline=>"#000000") if $i != 12;
    }
    $MM->createText(($x+$size/2+1),($y+$size/2+1),-text=>"$MPN[$i]",-anchor=>"center",-fill=>"#000000") if $MP[$i];
    $MM->createPolygon( ($x+$size/2),($y+$size/5), ($x+$size/2+12),($y+$size/5+8), ($x+$size/2-12),($y+$size/5+8), -fill=>"#000000", -width=>1) if $MP[$i] & 16;
    $MM->createPolygon( ($x+$size/2),($size+$y-$size/5), ($x+$size/2+12),($size+$y-$size/5-8), ($x+$size/2-12),($size+$y-$size/5-8), -fill=>"#000000", -width=>1) if $MP[$i] & 32;
  }
  $ShowMap = 1;
}

sub analyze_mm_html {
  my $startstring = 'top.hero.minimap({';
  my $endstring = ']},';
  my $startpos = index($html,$startstring);
  my $endpos = index($html,$endstring,$startpos+18);
  my $tmp = substr($html,$startpos+18,$endpos-$startpos-18);
  if ($startpos >0){
  for my $i (0 .. 24) {$MP[$i]=0;$MPN[$i]=0;}
    my @a = split("],",$tmp);
    foreach (@a) {
      $_ =~ s/\[//;
      my ($b,$c) = split(':',$_);
      my ($d,$e,$f) = split(",",$c);
      my $x = $d + 2;
      my $y = ($e + 2) * 5;
      my $pos = $x + $y;
      $MPN[$pos] = $b;
      $MP[$pos]  = $f;
    }
    $ShowMap = 1;
  }
}

